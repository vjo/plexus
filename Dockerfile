#
# Plexus SDN Controller Dockerfile
#

# Pull base image.
FROM pypy:3

# Define the build directory
ENV DEPLOYMENT /opt/plexus-deploy

# Define user and group to use
ENV PLEXUS_USER nobody
ENV PLEXUS_GROUP nogroup

# Populate the directory structure.
RUN mkdir ${DEPLOYMENT}
COPY setup.py ${DEPLOYMENT}
COPY pip-requires ${DEPLOYMENT}
COPY plexus ${DEPLOYMENT}/plexus
COPY sample_configuration ${DEPLOYMENT}/sample_configuration

# Set up plexus and install all dependencies.
RUN cd ${DEPLOYMENT} && \
        pip install -r pip-requires && \
        pypy ./setup.py install

# Change ownership of the log directory
RUN chown -R ${PLEXUS_USER}:${PLEXUS_GROUP} /var/log/plexus

# Define time zone, for logs
ENV TZ America/New_York

# Define ports
EXPOSE 6633 8080

# Change user, and run.
USER ${PLEXUS_USER}
WORKDIR ${DEPLOYMENT}
ENTRYPOINT pypy /opt/pypy/bin/ryu run --config-file /etc/plexus/ryu.conf
